import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Project } from './project';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  constructor(private httpClient: HttpClient) 
  { }

  getAllProjects() : Observable<Project[]>
  {
    alert("http://localhost:3600/projects/fetch-all-project")
    return this.httpClient.get<Project[]>("http://localhost:3600/projects/fetch-all-project");
  
  }

}
