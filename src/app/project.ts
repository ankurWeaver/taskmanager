export class Project {

    projectId: string;
    projectName: string;
    teamSize: number;


    constructor() {
        this.projectId = null;
        this.projectName = null;
        this.teamSize = 0;

    }
}
